# Inkscape extension duplicateReverseJoin

An extension to effectively convert a single line svg font to a stick font by duplicating, reversing and joining each selected path and subpath.

Read more on how I do that here: http://cutlings.wasbo.net/single-line-font-to-stick-font/